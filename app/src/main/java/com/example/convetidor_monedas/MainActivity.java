package com.example.convetidor_monedas;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView resultado;
    private RadioButton bn1,bn2,bn3,bn4,bn5;
    private EditText cantidad;
    private Button convertir;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //obtenemos los datos de la vista
        resultado = findViewById(R.id.txtresultado);
        bn1 = findViewById(R.id.rb1);
        bn2 = findViewById(R.id.rb2);
        bn3 = findViewById(R.id.rb3);
        bn4 = findViewById(R.id.rb4);
        bn5 = findViewById(R.id.rb5);
        convertir = findViewById(R.id.bnconvertir);
        cantidad = findViewById(R.id.txtcantidad);
        //damos la accion al boton
        convertir.setOnClickListener(this);

        //hago que el primer radioButton este selecionado
        bn1.setChecked(true);


    }

    @Override
    public void onClick(View v) {

        if (v == convertir){
            //verificamos que el campo de ingreso de datos este lleno
            if(cantidad.getText().toString().isEmpty()){
                cantidad.setError("Ingrese una cantidad");
                cantidad.requestFocus();
            }else{
                double res = 0;
                Double can = Double.parseDouble(cantidad.getText().toString());

                // lo utilisamos para que los decimales salgan con 3 numeros despues de la coma
                DecimalFormat formato1 = new DecimalFormat("0.000");

                //verifico si la primera opcion esta selecionada
                if(bn1.isChecked() == true){
                    res = (can * 0.848033);
                    resultado.setText(String.valueOf(formato1.format(res)) + " Euros");
                    resultado.setBackgroundColor(Color.parseColor("#9E9E9E"));

                }else if(bn2.isChecked() == true){ //verifico si la segunda opcion esta selecionada
                    res = can * 96.367386363636;
                    resultado.setText(String.valueOf(formato1.format(res)) + " Pesos argentinos");
                    resultado.setBackgroundColor(Color.parseColor("#82E0AA"));
                }else if(bn3.isChecked() == true){ //verifico si la tercer opcion esta selecionada
                    res = can * 3854.6954545455;
                    resultado.setText(String.valueOf(formato1.format(res)) + " Pesos Colombianos");
                    resultado.setBackgroundColor(Color.parseColor("#7FB3D5"));
                }
                else if(bn4.isChecked() == true){ //verifico si la cuarta opcion esta selecionada
                    res = can * 20.192704240779;
                    resultado.setText(String.valueOf(formato1.format(res)) + " Pesos Argentinos");
                    resultado.setBackgroundColor(Color.parseColor("#F0B27A"));
                }
                else if(bn5.isChecked() == true){ //verifico si la quinta opcion esta selecionada
                    res = can * 3.95;
                    resultado.setText(String.valueOf(formato1.format(res)) + " Soles");
                    resultado.setBackgroundColor(Color.parseColor("#28B463"));
                }
            }

        }

    }

    //metodo para combrobrar

}